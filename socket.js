const server = require('http').Server();
const io = require('socket.io')(server);

// server.listen('3000', function () {
// 	console.log('app running.');
// });

var Redis = require('ioredis');
var redis = new Redis();

redis.subscribe('test-channel');

redis.on('message', function(channel, message){
    
    message = JSON.parse(message);

    const paths = message.event.split('\\');

    const event = paths[paths.length-1];

    io.emit(`${channel}:${event}`, message.data);

    // socket.on('chat.message', function(message){
	// 	io.emit('chat.message', message.data);
	// });
});

const port = 4000;

server.listen(port, function(){
    console.log(`server is listening on port ${port}`);
});