<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    </head>
    <body>

    <div class='container' id="chatApp">
        <h1>Chatbox</h1>
        <ul>
            <li v-for="user in users" v-text="user"></li>
        </ul>
    </div>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.21/vue.min.js"></script>
  
  <script>

        var socket = io('http://localhost:4000');
        
        new Vue({
            el: '#chatApp',
            data: {
                users: []
            },
            created: function(){
                var $this = this;
                socket.on('test-channel:UserSignedUp', function(data){
                    $this.users.push(data.username);
                });
            }
        });

    </script>

    </body>
    
</html>
